"""Kubetest wrapper for the Kubernetes ``Role`` and ``ServiceAccount`` API Object."""

import logging

from kubernetes import client

from .api_object import ApiObject

log = logging.getLogger('kubetest')


class ServiceAccount(ApiObject):
    """Kubetest wrapper around a Kubernetes `ServiceAccount`_ API Object.

    The actual ``kubernetes.client.V1ServiceAccount`` instance that this
    wraps can be accessed via the ``obj`` instance member.

    This wrapper provides some convenient functionality around the
    API Object and provides some state management for the `ServiceAccount`_.

    .. _ServiceAccount:
        https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#service-account
    """

    obj_type = client.V1ServiceAccount

    api_clients = {
        'preferred': client.CoreV1Api,
        'v1': client.CoreV1Api,
    }

    def __str__(self):
        return str(self.obj)

    def __repr__(self):
        return self.__str__()

    def create(self, namespace=None):
        """Create the ServiceAccount under the given namespace.

        Args:
            namespace (str): The namespace to create the ServiceAccount under.
                If the ServiceAccount was loaded via the kubetest client, the
                namespace will already be set, so it is not needed here.
                Otherwise, the namespace will need to be provided.
        """
        if namespace is None:
            namespace = self.namespace

        log.info('creating serviceaccount "%s" in namespace "%s"', self.name, self.namespace)  # noqa
        log.debug('serviceaccount: %s', self.obj)

        self.obj = self.api_client.create_namespaced_service_account(
            namespace=namespace,
            body=self.obj,
        )

    def delete(self, options=None):
        """Delete the ServiceAccount.

        This method expects the ServiceAccount to have been loaded or otherwise
        assigned a namespace already. If it has not, the namespace will need
        to be set manually.

        Args:
             options (client.V1DeleteOptions): Options for ServiceAccount deletion.

        Returns:
            client.V1Status: The status of the delete operation.
        """
        if options is None:
            options = client.V1DeleteOptions()

        log.info('deleting serviceaccount "%s"', self.name)
        log.debug('delete options: %s', options)
        log.debug('serviceaccount: %s', self.obj)

        return self.api_client.delete_namespaced_service_account(
            namespace=self.namespace,
            name=self.name,
            body=options,
        )

    def refresh(self):
        """Refresh the underlying Kubernetes ServiceAccount resource."""
        self.obj = self.api_client.read_namespaced_service_account(
            namespace=self.namespace,
            name=self.name,
        )

    def is_ready(self):
        """Check if the ServiceAccount is in the ready state.

        ServiceAccounts do not have a "status" field to check, so we
        will measure their readiness status by whether or not they exist
        on the cluster.

        Returns:
            bool: True if in the ready state; False otherwise.
        """
        try:
            self.refresh()
        except client.rest.ApiException as e:
            # If we can no longer find the deployment, it is not ready.
            # If we get any other exception, raise it.
            if e.status == 404 and e.reason == 'Not Found':
                return False
            else:
                log.error('error refreshing service-account state')
                raise e
        else:
            return True


class Role(ApiObject):
    """Kubetest wrapper around a Kubernetes `Role`_ API Object.

    The actual ``kubernetes.client.V1Role`` instance that this
    wraps can be accessed via the ``obj`` instance member.

    This wrapper provides some convenient functionality around the
    API Object and provides some state management for the `Role`_.

    .. _Role:
        https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#role-v1-rbac-authorization-k8s-io
    """

    obj_type = client.V1Role

    api_clients = {
        'preferred': client.RbacAuthorizationV1Api,
        'rbac.authorization.k8s.io/v1': client.RbacAuthorizationV1Api,
        'rbac.authorization.k8s.io/v1alpha1': client.RbacAuthorizationV1alpha1Api,
        'rbac.authorization.k8s.io/v1beta1': client.RbacAuthorizationV1beta1Api,
    }

    def __str__(self):
        return str(self.obj)

    def __repr__(self):
        return self.__str__()

    def create(self, namespace=None):
        """Create the Role under the given namespace.

        Args:
            namespace (str): The namespace to create the Role under.
                If the Role was loaded via the kubetest client, the
                namespace will already be set, so it is not needed here.
                Otherwise, the namespace will need to be provided.
        """
        if namespace is None:
            namespace = self.namespace

        log.info('creating role "%s" in namespace "%s"', self.name, self.namespace)  # noqa
        log.debug('role: %s', self.obj)

        self.obj = self.api_client.create_namespaced_role(
            namespace=namespace,
            body=self.obj,
        )

    def delete(self, options=None):
        """Delete the Role.

        This method expects the Role to have been loaded or otherwise
        assigned a namespace already. If it has not, the namespace will need
        to be set manually.

        Args:
             options (client.V1DeleteOptions): Options for Role deletion.

        Returns:
            client.V1Status: The status of the delete operation.
        """
        if options is None:
            options = client.V1DeleteOptions()

        log.info('deleting role "%s"', self.name)
        log.debug('delete options: %s', options)
        log.debug('role: %s', self.obj)

        return self.api_client.delete_namespaced_role(
            namespace=self.namespace,
            name=self.name,
            body=options,
        )

    def refresh(self):
        """Refresh the underlying Kubernetes Role resource."""
        self.obj = self.api_client.read_namespaced_role(
            namespace=self.namespace,
            name=self.name,
        )

    def is_ready(self):
        """Check if the Role is in the ready state.

        Roles do not have a "status" field to check, so we
        will measure their readiness status by whether or not they exist
        on the cluster.

        Returns:
            bool: True if in the ready state; False otherwise.
        """
        try:
            self.refresh()
        except client.rest.ApiException as e:
            # If we can no longer find the deployment, it is not ready.
            # If we get any other exception, raise it.
            if e.status == 404 and e.reason == 'Not Found':
                return False
            else:
                log.error('error refreshing role state')
                raise e
        else:
            return True
